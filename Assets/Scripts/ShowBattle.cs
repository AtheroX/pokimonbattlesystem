﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowBattle : MonoBehaviour{

    #region Variables
    public CreateSOPokimon mPok, ePok;
    public Text mName, eName, mHPVal, eHPVal, mLvl, eLvl;
    public Slider mHPSlid, eHPSlid, mExp;
    public Image mSprite, eSprite;

    #endregion
    #region UnityMethods

    void Start() {
        setMyPokemonValues();
        setEnPokemonValues();

    }

    private void setMyPokemonValues() {
        mPok.XPneeded = Mathf.RoundToInt(3 * (Mathf.Pow(mPok.Level + 1,3))) / 5;
        mName.text = "Name: " + mPok.name;
        mHPSlid.maxValue = mPok.maxHP;
        mHPSlid.value = mPok.HP;
        mHPVal.text = "HP: " + mPok.HP + "/" + mPok.maxHP;
        mExp.maxValue = mPok.XPneeded;
        mExp.value = mPok.XP;
        mLvl.text = "LVL. " + mPok.Level;
        mSprite.sprite = mPok.sprite;
    }
    private void setEnPokemonValues() {
        eName.text = "Name: " + ePok.name;
        eHPSlid.maxValue = ePok.maxHP;
        eHPSlid.value = ePok.HP;
        eHPVal.text = "HP: " + ePok.HP + "/"+ePok.maxHP;
        eLvl.text = "LVL. " + ePok.Level;
        eSprite.sprite = ePok.sprite;
    }

    void Update(){
        
		
    }
    #endregion
}
