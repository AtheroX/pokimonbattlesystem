﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pokimon name", menuName = "new Pokimon",order = 2)]
public class CreateSOPokimon : ScriptableObject {

    #region Variables

    public new string name;
    public Sprite sprite;
    public int maxHP = 150, HP = 100, Level = 1, XP = 0, XPneeded = 0;
    public float AtackSpeed = 1f, Defense = 0f;
    
    #endregion
}
